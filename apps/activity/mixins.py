# 定义分页器
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
'''
分页器
'''

class TensquarePagination(PageNumberPagination):
    # 每页 个数
    page_size = 5
    # 查询参数的 名字 --页数的参数
    page_query_param = 'page'
    # 最大个数
    max_page_size = 10
    # 自定义 返回的 分页的内容
    def get_paginated_response(self, data):

        return Response({
            "count": self.page.paginator.count,
            "results": data,
            "page": self.page.number,
            "pages": self.page.paginator.num_pages,
            "pagesize": self.page_size

        })
