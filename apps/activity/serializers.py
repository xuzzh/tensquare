from rest_framework import serializers

from apps.activity.models import Gathering


class GatheringsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Gathering
        fields = ['id', 'name', 'image', 'city', 'starttime', 'endrolltime', 'users']

class GatherSerializer(serializers.ModelSerializer):
    class Meta:
        model = Gathering
        fields = '__all__'
