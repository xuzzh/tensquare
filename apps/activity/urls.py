"""
app URL Configuration
"""
from django.conf.urls import url
from . import views

urlpatterns = [
    # 活动列表
    url(r'^gatherings/$', views.GatheringsView.as_view()),
    # 活动详情
    url(r'^gatherings/(?P<pk>[^/.]+)/$', views.GatherView.as_view()),
    # 报名活动
    url(r"^gatherings/(?P<pk>[^/.]+)/join/$", views.GatherJoinView.as_view()),

]
