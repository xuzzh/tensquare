from datetime import datetime

from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.generics import ListAPIView, RetrieveAPIView, GenericAPIView

from apps.activity.mixins import TensquarePagination
from apps.activity.serializers import GatheringsSerializer, GatherSerializer
from apps.activity.models import Gathering

class GatheringsView(ListAPIView):
    serializer_class = GatheringsSerializer
    # state=1 可见　=2　不可见
    queryset = Gathering.objects.filter(state=1)
    pagination_class = TensquarePagination

class GatherView(RetrieveAPIView):
    serializer_class = GatherSerializer
    queryset = Gathering.objects.filter(state=1)
    pagination_class = TensquarePagination

class GatherJoinView(GenericAPIView):
    queryset = Gathering.objects.filter(state=1)
    permission_classes = [IsAuthenticated]

    def post(self, request, pk):
        user = request.user
        gathering = self.get_object()
        now = datetime.now()
        endtime = gathering.endrolltime.replace(tzinfo=None)
        if endtime < now:
            return Response({'success': False, 'message': '报名时间已过'}, status=400)
        else:
            if user in gathering.users.all():
                gathering.users.remove(user)
                gathering.save()
                return Response({'success': True, 'message': '取消成功'})
            else:
                gathering.users.add(user)
                gathering.save()
                return Response({'success': True, 'message': '参加成功'})
