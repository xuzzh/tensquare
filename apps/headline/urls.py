from django.conf.urls import url
from rest_framework.routers import SimpleRouter


from . import views
from rest_framework import routers

router = routers.SimpleRouter()

urlpatterns = [

]
router = SimpleRouter()
# router.register(r'headline', views.ArticleViewSet)
# 获取首页左侧栏中的频道列表
router.register(r'channels', views.ChannelViewSet)
# router.register('articles/search', views.ArticleSearchViewSet, base_name='articles_search')
urlpatterns += router.urls