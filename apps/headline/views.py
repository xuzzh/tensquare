from django import http
from django.conf import settings
from django.shortcuts import render
from django.views import View
from rest_framework.decorators import action
from rest_framework.response import Response

from .mixin import MeiduoPagination
from .models import Channel, Article
# Create your views here.
from rest_framework.viewsets import ModelViewSet

from .serialzers import ChannelsSerializers, ArticleSerializerForList, LabelsSerializer
from .models import Label
# 上传图片
# from tensquare.settings import FDFS_BASE_URL


class ChannelViewSet(ModelViewSet):
    queryset = Channel.objects.all()
    serializer_class = ChannelsSerializers
    pagination_class = MeiduoPagination