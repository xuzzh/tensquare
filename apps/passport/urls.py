"""
app URL Configuration
"""
from django.conf.urls import url
from rest_framework_jwt.views import obtain_jwt_token

from . import views

urlpatterns = [
    # 短信验证码
    url(r'^sms_codes/(?P<mobile>1[3-9]\d{9})/$', views.SMSCodeView.as_view()),
    # 注册用户
    url(r'^users/$', views.UserCreateView.as_view()),
    # 登录认证
    url(r'^authorizations/$', obtain_jwt_token),

]
