from rest_framework import serializers

from .models import City, Enterprise, Recruit


class CitySerializer(serializers.ModelSerializer):
    class Meta:
        model = City
        fields = '__all__'


class EnterpriseSerializer(serializers.ModelSerializer):
    # 外键属性有related_name，外键不能使用recruit_set
    # recruit_set = serializers.PrimaryKeyRelatedField(read_only=True, many=True)
    recruits = serializers.PrimaryKeyRelatedField(read_only=True, many=True)

    class Meta:
        model = Enterprise
        # fields = '__all__'
        fields = ['id', 'name', 'labels', 'logo', 'recruits', 'summary']


class RecruitSerializer(serializers.ModelSerializer):
    enterprise = EnterpriseSerializer()

    class Meta:
        model = Recruit
        fields = '__all__'


class EnterpriseDetailSerializer(serializers.ModelSerializer):
    recruits = RecruitSerializer(read_only=True, many=True)

    class Meta:
        model = Enterprise
        fields = '__all__'


class RecruitDetailSerializer(serializers.ModelSerializer):
    enterprise = EnterpriseDetailSerializer()

    class Meta:
        model = Recruit
        fields = '__all__'
