"""
app URL Configuration
"""
from django.conf.urls import url
from rest_framework.routers import DefaultRouter

from . import views

urlpatterns = [
    # 热门城市
    url(r'^city/hotlist/$', views.CityView.as_view()),
    # 热门企业
    url(r'^enterprise/search/hotlist/$', views.EnterpriseHotListView.as_view()),

]

router = DefaultRouter()
# 企业详情/访问次数/收藏
router.register(r'enterprise', views.EnterpriseDetailViewSet, 'enterprise')
# 职位详情/访问次数/收藏
router.register(r'recruits', views.RecruitDetailViewSet, 'recruit')

urlpatterns += router.urls
