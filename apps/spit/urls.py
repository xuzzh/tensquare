"""
app URL Configuration
"""
from rest_framework.routers import SimpleRouter

from apps.spit import views

urlpatterns = [

]
router = SimpleRouter()
router.register(r'spit', views.SpitViewSet)
urlpatterns += router.urls
