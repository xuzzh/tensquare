from rest_framework import serializers

from apps.headline.models import Article
from apps.question.models import Label, Reply, Question
from apps.recruit.models import Enterprise
from apps.user.models import User

# ---------------------以下为第三层嵌套　序列化器---------------------

# 问答序列化器中--用户的信息  -这个序列化器要在最上面,方便其他调用
class UserReplySerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        # 需要获取到用户的　名字　和头像
        fields = ['id', 'username', 'avatar']

# 父级评论及子级评论　
class ReplySerializerForSubAndParent(serializers.ModelSerializer):
    #  注意: 这里　需要的字段　是user  不是users　!!!
    user = UserReplySerializer(read_only=True)

    class Meta:
        model = Reply
        fields = ["id", "content", "createtime", "useful_count", "unuseful_count", "user"]





# ---------------------以下为第二层嵌套　序列化器---------------------

# 企业序列化器　　　前端页面: 个人中心－我的收藏－　收藏的企业
class EnterpriseSerializerSimple(serializers.ModelSerializer):
    # 用户信息　　'隐藏外建属性需自定义'
    users = UserReplySerializer(read_only=True)
    class Meta:
        # 招聘信息
        model = Enterprise
        fields = '__all__'

# 头条序列化器　　　　　前端页面: 个人中心－我的收藏－　收藏的文章
class ArticleSerializerForList(serializers.ModelSerializer):
    # 用户信息　　'隐藏外建属性需自定义'
    users = UserReplySerializer(read_only=True)
    # 被那些用户收藏   '隐藏外建属性需自定义'
    collected = serializers.BooleanField(default=False)
    class Meta:
        # 文章
        model = Article
        fields = '__all__'

# 问答序列化器　　　　前端页面: 个人中心－我的回答
class ReplySerializerForList(serializers.ModelSerializer):
    # 用户信息     '隐藏外建属性需自定义'
    users = UserReplySerializer(read_only=True)
    # 父级与子级评论  '隐藏外建属性需自定义'
    subs = ReplySerializerForSubAndParent(read_only=True, many=True)
    parent = ReplySerializerForSubAndParent(read_only=True)
    class Meta:
        model = Reply
        fields = '__all__'

# 问题序列化器　　　前端页面: 个人中心－我的提问
class QuestionsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Question
        fields = '__all__'

# 标签序列化器   前端页面: 个人中心－我的关注－关注的标签
class LabelsSerializer(serializers.ModelSerializer):

    class Meta:
        model = Label
        fields = '__all__'







# ---------------------以下为第一层嵌套　序列化器---------------------

# 用户序列化器　　前端页面: 个人中心－我的主页
class UserDetailSerializer(serializers.ModelSerializer):
    # 标签序列化器
    Labels = LabelsSerializer(required=False, many=True)
    # 问题序列化器
    questions = QuestionsSerializer(many=True)
    # 问答序列化器
    answer_question = ReplySerializerForList(read_only=True, many=True)
    # 文章序列化器
    collected_articles = ArticleSerializerForList(read_only=True, many=True)
    # 企业序列化器
    enterpises = EnterpriseSerializerSimple(read_only=True, many=True)
    class Meta:
        model = User
        fields = '__all__'

# 定义 修改密码的 序列化器
class UserPwdSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'
        extra_kwargs={
            # 密码安全问题 不能读取
            'password':{
                'write_only':True
            }
        }

    def update(self, instance, validated_data):
        # super继承父类方法  instance 修改的对象
        instance = super().update(instance, validated_data)
        # set_password 是设置加密密码   check_password 是获取
        instance.set_password(validated_data.get('password'))
        # 修改完后 保存到数据库
        instance.save()
        # 向视图返回 instance 对象
        return instance