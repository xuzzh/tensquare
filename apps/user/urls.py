"""
app URL Configuration
"""
from django.conf.urls import url
from . import views

urlpatterns = [
    # 个人中心
    url(r'^user/$', views.UserDetailView.as_view()),
    # 关注和取消关注
    url(r'^users/like/(?P<userid>[^/.]+)/$', views.UserLikeView.as_view()),
    # 修改密码
    url(r'^user/password/$', views.UserPasswordView.as_view()),
]
